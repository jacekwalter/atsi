import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Shouts = new Mongo.Collection('shouts');

if (Meteor.isServer) {
  Meteor.publish('shouts', function shoutsPublication() {
    return Shouts.find({});
  });
}

Meteor.methods({
  'shouts.insert'(text) {
    check(text, String);

    if (! this.userId) {
	  Shouts.insert({
		text,
		createdAt: new Date(),
		username: 'Anonymous',
	  });
    } else {
	  Shouts.insert({
        text,
        createdAt: new Date(),
        owner: this.userId,
        username: Meteor.users.findOne(this.userId).username,
      });
	}
    
  },
  'shouts.remove'(shoutId) {
    Shouts.remove(shoutId);
  },
});
