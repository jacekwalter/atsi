import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './menu.html';

Template.menu.events({
  'click #shoutcast': function(){
    Router.go('/');
  },
  'click #about': function(){
    Router.go('/about');
  },
});


