import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { Shouts } from '../api/shouts.js';

import './shout.js';
import './home.html';

Template.home.onCreated(function bodyOnCreated() {
  this.state = new ReactiveDict();
  Meteor.subscribe('shouts');
});

Template.home.helpers({
  shouts() {
    return Shouts.find({}, { sort: { createdAt: -1 } });
  },
});

Template.home.events({
  'submit .new-shout'(event) {

    event.preventDefault();
    const target = event.target;
    const text = target.text.value;
    
    Meteor.call('shouts.insert', text);

    target.text.value = '';
  },
});
