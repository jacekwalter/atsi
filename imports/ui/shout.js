import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './shout.html';

const moderators = ["as@as.as"];

Template.shout.helpers({
  isOwner() {
    return this.owner === Meteor.userId();
  },
  isModerator() {
	return Meteor.user() && moderators.indexOf(Meteor.user().username) > -1;
  },
});

Template.shout.events({
  'click .delete'() {
    Meteor.call('shouts.remove', this._id);
  },
});
